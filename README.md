# GNS3 Container

## Overview

This project aims to build a GNS3 server inside a container, which can be executed in any Docker, Podman, or Kubernetes environment. The goal is to provide an alternative to the traditional VM approach for running the GNS3 server. The benefits include:

    Easier deployment
    No need to run nested hypervisors, at least on Linux

Additionally, the user will be able to connect to the GNS3 container and interact with the nodes running in GNS3 directly from their desktop. This will be facilitated by providing the user the ability to "dial in" to the GNS3 main core network.