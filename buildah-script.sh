#!/bin/bash

export CONTAINER_IMAGE="docker.io/linuxjoergen/gns3-container"
export CONTAINER_VERSION="0.9.2"
export GNS3_VERSION=3.0.2


set -x

buildah rm gns3-container

# Create the base container from the official Debian image
buildah from --name gns3-container debian:trixie

# Set the working directory
buildah config --workingdir /gns3-container gns3-container

# Ensure no recommended packages are installed to keep the container as small possible
buildah run gns3-container -- sh -c 'apt-config dump | grep -we Recommends -e Suggests | sed s/1/0/ | tee /etc/apt/apt.conf.d/999norecommend'

# Ensure non-free gets added to be able to install Dynamips. Dynamips is GPL, so I don't know why it lives in non-free
buildah run gns3-container -- sh -c  'sed -i "s/\bmain\b/main non-free non-free-firmware/g" /etc/apt/sources.list.d/debian.sources'

# Install any needed packages. GNS3 packets will be installed later from GNS3 ppa.
buildah run gns3-container -- apt-get update
buildah run gns3-container -- apt-get install -y \
    sysvbanner \
    python3 \
    python3-pip \
    procps \
    psmisc \
    openssh-server \
    iproute2 \
    iptables \
    frr \
    wget \
    nano \
    uuid \
    openvpn \
    qemu-system-x86 \
    qemu-utils \
    netcat-openbsd \
    dnsmasq \
    net-tools \
    docker.io \
    docker-cli \
    tigervnc-standalone-server \
    qemu-system-modules-spice \
    busybox-static \
    supervisor

# Install gns3 server using Python PIP
buildah run gns3-container -- pip3 install --break-system-package gns3-server==${GNS3_VERSION}

# Copy the entire rootfs into the container
buildah copy gns3-container rootfs /

# Prepare the container with a simple self explaining shell
buildah run gns3-container -- sh -c '
     chmod +x /entrypoint.sh &&\
     useradd gns3 -d /gns3-container &&\
     usermod -d /gns3-container -m gns3 &&\
     usermod -aG docker gns3 &&\
     chown -R gns3:gns3 /gns3-container &&\
     apt-get update &&\
     yes yes | apt-get install -y \
         ubridge \
         dynamips \
         vpcs'

# Expose necessary ports
buildah config --port 80 gns3-container
buildah config --port 443 gns3-container
buildah config --port 3080 gns3-container

# Set the entrypoint and command
buildah config --entrypoint '["/entrypoint.sh"]' gns3-container
buildah config --cmd '["/usr/bin/supervisord", "-n", "-c", "/etc/supervisor/supervisord.conf"]' gns3-container

# Commit the container to an image
buildah commit gns3-container "${CONTAINER_IMAGE}":"${CONTAINER_VERSION}"
buildah tag "${CONTAINER_IMAGE}":"${CONTAINER_VERSION}" "${CONTAINER_IMAGE}":latest
