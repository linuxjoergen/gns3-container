#!/bin/bash

banner "It's as"
banner "easy as"
banner "1.. 2.. "
banner "GNS3!!!"


echo Configure PKI...
/usr/local/bin/generate_pki.sh

echo Generate Diffie Hellman key for OpenVPN
openssl dhparam -out /etc/openvpn/dh2048.pem 2048

# Create the virtual bridge used by GNS3 server. The bridge originates
# from libvirt, but since we don't need the rest of the libvirt toolstack
# we will just create the bridge manually during boot.
ip link add name virbr0 type bridge
ip link set dev virbr0 up

# Prepare the OpenVPN TAP adapter, and make it a member of virbr0
ip tuntap add mode tap tap-openvpn0
ip link set up dev tap-openvpn0
ip link set tap-openvpn0 master virbr0


# Check whether these variables are defined as environment variables.
# If not: Set some defaults:
test -z "${BRIDGE_ADDRESS}"  && BRIDGE_ADDRESS="10.254.0.1/16"
test -z "${OPENVPN_NETWORK}" && OPENVPN_NETWORK="10.253.0.0 255.255.255.0"
test -z "${DHCP_RANGE}"      && DHCP_RANGE="10.254.0.100,10.254.0.150"
test -z "${DHCP_DNS}"        && DHCP_DNS="1.1.1.1"
test -z "${DHCP_NTP}"        && DHCP_NTP="216.40.34.37" # ntp.pool.org resolved.


# Since the above variables contains dots and slashes, let's escape them
# so that we can use them with sed
escaped_bridge_address=$(printf '%s\n' "$BRIDGE_ADDRESS" | sed -e 's/[\/&]/\\&/g')
escaped_openvpn_network=$(printf '%s\n' "$OPENVPN_NETWORK" | sed -e 's/[\/&]/\\&/g')
escaped_dhcp_range=$(printf '%s\n' "$DHCP_RANGE" | sed -e 's/[\/&]/\\&/g')
escaped_dhcp_dns=$(printf '%s\n' "$DHCP_DNS" | sed -e 's/[\/&]/\\&/g')
escaped_dhcp_ntp=$(printf '%s\n' "$DHCP_NTP" | sed -e 's/[\/&]/\\&/g')


# And finally update these variables in their respective files:
sed -i "s/{{openvpn_network}}/$escaped_openvpn_network/g" /usr/local/bin/route-watchdog.sh
sed -i "s/{{dhcp_range}}/$escaped_dhcp_range/g" /etc/dnsmasq.conf
sed -i "s/{{dhcp_dns}}/$escaped_dhcp_dns/g" /etc/dnsmasq.conf
sed -i "s/{{dhcp_ntp}}/$escaped_dhcp_ntp/g" /etc/dnsmasq.conf


# The virbro will have it's address set
ip address add $BRIDGE_ADDRESS dev virbr0


# Update ospfd.conf with the the network of virbr0 to ensure ospf
# learn routes from virbr0
virbr0_network=$(ip route show dev virbr0 | awk '{print $1}')
escaped_virbr0_network=$(printf '%s\n' "$virbr0_network" | sed -e 's/[\/&]/\\&/g')
sed -i "s/{{virbr0_network}}/$escaped_virbr0_network/g" /etc/frr/ospfd.conf

# Ensure, that the GNS3 nodes can see the outside world:
default_route_interface=$(ip route | awk '/default/ {print $5}')
iptables -t nat -A POSTROUTING -o ${default_route_interface} -j MASQUERADE

exec $@