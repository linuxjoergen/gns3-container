#!/bin/bash

client_id=$(uuid)

openssl genrsa -out /tmp/openvpn-client.key 1024
openssl req -new -key /tmp/openvpn-client.key -out /tmp/openvpn-client.csr -subj "/C=DK/ST=Jylland/L=OpenVPN Client/O=ZealSoft/OU=R&D/CN=GNS3 Container $client_id"
openssl x509 -req -days 3650 -in /tmp/openvpn-client.csr -CA /gns3-container/certificates/gns3-container-ca.pem -CAkey /gns3-container/certificates/gns3-container-ca.key -set_serial 01 -out /tmp/openvpn-client.cer 2> /dev/null


cat <<EOF
client
dev tun
proto tcp
remote localhost 1194
resolv-retry infinite
nobind
persist-key
persist-tun
cipher AES-256-CBC
verb 3
mssfix 1456

EOF

echo "<ca>"
cat /gns3-container/certificates/gns3-container-ca.pem
echo "</ca>"
echo "<cert>"
cat /tmp/openvpn-client.cer
echo "</cert>"
echo "<key>"
cat /tmp/openvpn-client.key
echo "</key>"
