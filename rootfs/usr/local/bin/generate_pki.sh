# Certificates are stored in /gns3-container. If user wants to use own certificates, she would just need
# to mount /gns3-container as a persistent datastore and put the certificates in here.

# If no certificates is found, like in the case of first boot: Certificates will be generated automatically

CERTIFICATE_PATH=/gns3-container/certificates

if [ ! -d ${CERTIFICATE_PATH} ]; then
    echo Certificates folder not found. Creating certificates in ${CERTIFICATE_PATH}
    mkdir -p ${CERTIFICATE_PATH}
    openssl req -new -newkey rsa:1024 -nodes -out ${CERTIFICATE_PATH}/gns3-container-ca.csr -keyout ${CERTIFICATE_PATH}/gns3-container-ca.key -subj "/C=DK/ST=Jylland/L=Aarhus/O=GNS3-container/OU=GNS3/CN=GNS3-Container Certificate Authority"
    openssl x509 -trustout -signkey ${CERTIFICATE_PATH}/gns3-container-ca.key -days 3650 -req -in ${CERTIFICATE_PATH}/gns3-container-ca.csr -out ${CERTIFICATE_PATH}/gns3-container-ca.pem

    openssl genrsa -out ${CERTIFICATE_PATH}/gns3-container-server.key 1024
    openssl req -new -key ${CERTIFICATE_PATH}/gns3-container-server.key -out ${CERTIFICATE_PATH}/gns3-container-server.csr -subj "/C=DK/ST=Jylland/L=gns3-container/O=ZealSoft/OU=R&D/CN=GNS3 Container"
    openssl x509 -req -days 3650 -in ${CERTIFICATE_PATH}/gns3-container-server.csr -CA ${CERTIFICATE_PATH}/gns3-container-ca.pem -CAkey ${CERTIFICATE_PATH}/gns3-container-ca.key -set_serial 01 -out ${CERTIFICATE_PATH}/gns3-container-server.cer

    # Ensure we have the format in DER format as well as required by SoftEther
    openssl x509 -outform der -in ${CERTIFICATE_PATH}/gns3-container-ca.pem -out ${CERTIFICATE_PATH}/gns3-container-ca.der

    chmod +r ${CERTIFICATE_PATH}/*.key
else
    echo Certificate folder ${CERTIFICATE_COMMON_NAME} in place. Skipping certificate generation...
fi
