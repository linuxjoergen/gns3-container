#!/bin/bash

# This script looks for updated ospf routes. If routes has changed, we need to inform
# OpenVPN clients:
#
# Update the openvpnserver.conf, so all routes associated with virbr0 are pushed to clients.
#

OPENVPN_CONFIG_FILE=/etc/openvpn/server/server.conf
BASECONFIG='
server {{openvpn_network}}
port 1194
proto tcp
duplicate-cn
topology subnet
mssfix 1456
dev tun
ca /gns3-container/certificates/gns3-container-ca.pem
cert /gns3-container/certificates/gns3-container-server.cer
key /gns3-container/certificates/gns3-container-server.key
dh /etc/openvpn/dh2048.pem
keepalive 10 120
user nobody
group nogroup
persist-key
persist-tun
status openvpn-status.log
verb 3
'

while true; do
    # All routes associated with virbr0 should be announced to the OpenVPN client
    # Exclude 127.0.0.1 - For some reason some routers announce 127.0.0.1 as an OSPF route.
    ROUTES="$(route -n  | grep "virbr0$" | grep -v "127.0.0.1" | awk '{print "push \"route "$1,$3"\""}')"
    CONFIG="${BASECONFIG}${ROUTES}"

   # Check for changes in $now and $before
   if [ "${CONFIG}" != "$(cat $OPENVPN_CONFIG_FILE)" ]; then
       echo OSPF routes have changed. Updating $OPENVPN_CONFIG_FILE
       echo "$CONFIG" > $OPENVPN_CONFIG_FILE
       echo Restarting OpenVPN Server
       supervisorctl restart openvpn_server
   fi
   sleep 15

done
