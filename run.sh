#!/bin/bash

HOSTNAME="gns3-container"
BRIDGE_ADDRESS="10.254.0.1/16"
OPENVPN_NETWORK="10.252.0.0 255.255.255.0"
DHCP_RANGE="10.254.0.100,10.254.0.150"
DHCP_DNS="1.1.1.1"
DHCP_NTP="216.40.34.37" # ntp.pool.org resolved


docker run \
  --rm \
  --name gns3-container \
  --privileged \
  -p 1194:1194 \
  -e BRIDGE_ADDRESS="${BRIDGE_ADDRESS}" \
  -e OPENVPN_NETWORK="${OPENVPN_NETWORK}" \
  -e DHCP_RANGE="${DHCP_RANGE}" \
  -e DHCP_DNS="${DHCP_DNS}" \
  -e DHCP_NTP="${DHCP_NTP}" \
  -v ${HOME}/GNS3:/gns3-container \
  -v ${HOME}/GNS3/var_lib_docker:/var/lib/docker \
  --hostname "${HOSTNAME}" \
  docker.io/linuxjoergen/gns3-container:latest
